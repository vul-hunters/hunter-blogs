+++
date = "2017-04-12T18:00:15+08:00"
draft = true
title = "OWASP的TOP10 2017版解读"
author = "HuangJacky"
categories = ["安全"]
tags = ["OWASP"]
description = "针对OWASP最新发布2017版TOP10进行解读,重点讲解其中漏洞类型发生变化的部分."

+++

### 0x00 前言

### 0x05 参考资料
1. [OWASP TOP10 2017RC英文原版](https://raw.githubusercontent.com/OWASP/Top10/master/2017/OWASP%20Top%2010%20-%202017%20RC1-English.pdf)