+++
date = "2017-04-02T09:28:03+08:00"
title = "开源中国Pages自动发布小介绍"
description = "如何将pages发布过程中不需要人参与部分实现自动化"
draft = false
author = "huangjacky"
truncated = true
summary = "简单的说明一下Pages自动发布的一个思路,本例子中的接口主要是tornado实现,同时你还需要一台自己的服务器"
categories = ["工具"]
tags = ["python"]

+++
## 介绍
开源中国也开启pages的功能,支持静态写博.和github.io稍微有一些不同的是,开源中国可以指定仓库中某一个分支为pages所使用的.
而我们使用工具主要是**hugo**, 它可以自动将一些markdown文件和主题一起生成我们所看到的博客的静态html页面.
## 分支介绍
那么我们博客就采用master和blogs两个常驻分支:
### master
主要存放原始markdown和hugo所需要的文件.每个作者写完自己的文章之后都需要合并到master分支上.
### blogs
这个分支被pages所采用,主要是hugo程序生成后html等文件,因此都是程序自动生成,不需要手动编辑此分支中任意文件
### huangjacky, yoyoyang
这些都是每一个作者自己的分支, 方便作者在不同电脑上面协作.

## 自动发布介绍
发表文章正常的流程是:
1. 获取最新的master分支, 这里有所有人的文章
2. 使用hugo生成整站pages的html
3. 将生成的html提交到blogs分支中.

这里可能会比较繁琐了.正常的作者需要 提交修改到自己分支, 合并到master, 使用hugo生成最新的pages, 提交到blogs分支.

由于开源中国提供push的hook,当每一个分支有人push的时候,可以向指定接口发送一个post请求, 那么从上面三个步骤中第一步之后都可以程序自动实现, 每个作者只需要如何合并自己的分支到master中就好了.

自动发布博客的程序主要实现以下几个功能:
1. 接口判断post过来数据是否为push hook,同时判断ref是否为master分支
2. 本地仓库执行git pull origin master, 拉取最新的master分支
3. 本地执行hugo build
4. 切换到pages目录, 执行 git push origin blogs

一切就搞定,整个流程中将无需人为参与部分都自动化,减少工作量.


