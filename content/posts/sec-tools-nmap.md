+++
date = "2017-04-17T10:44:28+08:00"
draft = false
title = "安全工具 - nmap用法"
author = "HuangJacky"
description = "基础篇 - 主要讲解一些nmap的基本用法"
categories = ["安全工具"]
tags = ["安全", "nmap"]

+++
## 0x00 序言
工欲善其事, 必先利其器. 当然这系列的文章都是偏入门级的, 主要是方便我自己熟悉相关的工具, 以及日后使用过程中, 自己有个地方作为参考.
nmap作为安全圈的神器, 在黑客帝国里面也有露面, 因此你可以想象到它的用途了吧
## 0x01 用法
### 扫描目标
nmap接受的输入可以有单IP,域名,网段和IP列表文件.
```bash
# 扫描单个ip
nmap 127.0.0.1
# 扫描一个ip端
nmap 192.168.2.1-25
# 扫描一个子网段
nmap 192.168.2.0/24
# 扫描一个域名
nmap www.test.com
# 扫描文件列表中IP
nmap -iL ips.txt
# 排除指定ip, 不扫描192.168.3.1
nmap 192.168.3.0/24 --exclude 192.168.3.1
```
### 端口选项
默认nmap会扫描1000个端口, 但是可以用过-p参数来设置.
```bash
# 扫描常见的100哥端口, 快速扫描模式
nmap -F 192.168.3.1
# 扫描指定端口, 可以使单个也可以是多个
nmap -p22,23,443 192.168.3.1
# 扫描所有端口1-65535
nmap -p- 192.168.3.1
```
### 主机发现
nmap在对主机进行扫描前, 通常会通过ping命令来发现主机是否存活, 主要用来优化扫描速度. 目前很多服务器都屏蔽了ping包, 因此这里可能造成一定的误报.
#### -sP
通过ping来发现主机
#### -P0, -Pn
跳过ping发现过程
#### -PS
通过TCP SYN ping来发现主机
#### -PA
通过TCP ACK ping来发现主机
#### -PU
通过UDP ping来发现主机
#### -PE; -PP; -PM
通过ICMP ping来发现主机
#### -PR
通过ARP ping来发现主机
### 端口扫描技术
nmap提供多种端口扫描技术,一般来说一次只能使用一种扫描技术,但是sU可以和其他扫描技术并用.
#### -sS
通过TCP SYN扫描, 也是nmap默认的扫描类型, 也叫做半开放式扫描, 每秒钟可以扫描上千个端口.
#### -sT
通过TCP connect扫描, 当-sS不能使用的时候, nmap将会采用-sT方式来扫描.
#### -sU
通过UDP扫描. 目前有很多服务都使用UDP协议, 但是UDP因为没有常驻监听端口, 所以很多人都会忽略.
#### -sN
通过TCP NULL进行扫描
#### -sF
通过TCP FIN进行扫描
#### -sX
通过TCP Xmas进行扫描
#### -sA
通过TCP ACK进行扫描, 主要用来发现防火墙规则
### 版本和系统指纹探测
当端口连接上后, 通过报文包内容, nmap内置多种匹配规则能够发现目标端口和主机的服务以及系统版本等有效信息.
#### -sV
打开版本探测, 结合-version-intensity 来设置版本探测的强度,0~9可以设置,默认为7
#### -O
打开操作系统探测
#### -A
同时打开版本探测和操作系统探测, 因此-A 是上面两个功能的集合.
### 输出
nmap主要提供两种输出格式,其中oN是正常的输出,oX是XML格式,可以用于后期程序利用. 为了简化,nmap还提供oA来简化输出选项, 这个可以输出所有格式.
## 0x02 常用用法
### 快速全端口扫描
```bash
nmap -p 1-65535 -sV -sS -T4 target
```
### 详细快速扫描
```bash
nmap -v -sS -A -T4 target
```
### 快速扫描指定端口
```bash
nmap -sV -p 139,445 -oG grep-output.txt 10.0.1.0/24
```
### 将扫描结果导出成HTML
```bash
nmap -sS -sV -T5 10.0.1.99 --webxml -oX - | xsltproc --output file.html -
```

## 0x03 附录
1. [nmap官方参考指南](https://nmap.org/man/zh/index.html)
2. [kali的nmap教程](http://tools.kali.org/information-gathering/nmap)
3. [nmap手册1](https://hackertarget.com/nmap-cheatsheet-a-quick-reference-guide/)
4. [nmap手册2](https://highon.coffee/blog/nmap-cheat-sheet/)
