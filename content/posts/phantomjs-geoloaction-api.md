+++
date = "2017-03-26T21:20:23+08:00"
title = "phantomjs - 支持地理位置API"
description = "如何在phantomjs中模拟支持地理位置的小技巧"
draft = false
author = "huangjacky"
categories = ["编程"]
tags = ["phantomjs"]

+++
目前phantomjs并不支持GeolocationAPI,但是我们可以手动编写一个js文件, 用自己的代码实现常见的Geolocation API.

```js
var webPage = require('webpage');
var page = webPage.create();
//TODO other codes
//初始化之前插入一些常用js utils
page.onInitialized = function(){
    //模拟地理定位.
    page.injectJs("fake-location.js");
};
```
fake-location.js的代码也比较简单:

```js
/**
 * @author huangjacky
 * @desc 模拟实现浏览器的获取地理位置的api
 */

window.navigator.geolocation = {
    getCurrentPosition: function (success, failure) {
        success({
            coords: {
                //模拟华中科技大学产学研基地
                latitude: 22.52902,
                longitude: 113.94376
            }, timestamp: Date.now()
        });
    },
    watchPosition: function(success, failure){
        success({
            coords: {
                //模拟华中科技大学产学研基地
                latitude: 22.52902,
                longitude: 113.94376
            }, timestamp: Date.now()
        });
    }
};
```


