+++
title = "SQL中显示所有相同的记录"
description = "一个MySQL的小秘密"
draft = false
author = "huangjacky"
date = "2017-03-26T14:33:16+08:00"
categories = ["编程"]
tags = ["MySQL"]

+++
```sql
select * from t_cgi
	 WHERE (cHost,cUri) in (
		select cHost,cUri from t_cgi GROUP BY cHost,cUri HAVING COUNT(cHash)> 1
	) ORDER BY CONCAT(cHost,cUri)
```
因为需要统计,所以必须要用**GROUP BY**, 然后用**HAVING COUNT**进行判断数据记录数, 这里有一个问题,就是因为是两个字段,所以在获取时候的WHERE IN需要用括号括起来.
附录:表结构

```sql
CREATE TABLE t_cgi(
  cHash CHAR(32) UNIQUE,
  cHost VARCHAR(32) NOT NULL ,
  cUri VARCHAR(64) NOT NULL DEFAULT '/',
  cRaw TEXT ,
  cTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (cHash)
)
```



