+++
date = "2017-03-28T16:07:45+08:00"
title = "图片隐写术介绍"
description = "介绍常用的一个图片隐写术的用法和工具"
author = "huangjacky"
draft = false
categories = ["安全"]
tags = ["隐写"]

+++
隐写术是将文本或者文件隐藏到图片文件中,从而实现信息的屏蔽.

本文主要介绍关于Python的steganography包以及相关的用法,以及编程中使用说明
### 安装
```bash
pip install steganography
```

### 用法
#### 命令行
steganography包安装之后,命令行中会存在一个steganography的可执行脚本.
##### 加密
```bash
steganography -e 原始图片 加密后的图片 "加密的文本"
```
##### 加密
```bash
steganography -d 加密后的图片
```

#### 代码引用
```python
from steganography.steganography import Steganography

# hide text to image
path = "/tmp/image/input.jpg"
output_path = "/tmp/image/output.jpg"
text = 'The quick brown fox jumps over the lazy dog.'
Steganography.encode(path, output_path, text)

# read secret text from image
secret_text = Steganography.decode(output_path)
```

以后都这样尝试了.
### 后续
在使用过程中,发现如果是windows存进去的中文文本解码之后可能会存在乱码的情况,这个就是编码的问题了,而且存入的内容没有经过加密的内容直接存入还是不安全.
经过以上分析,我决定自己重写一个脚本.
内容如下:
```python
#! /usr/local/bin/python
# -*- coding:utf-8 -*-
import sys
from steganography.steganography import Steganography
import argparse
import base64
import chardet
from Crypto.Cipher import AES
import os
reload(sys)
sys.setdefaultencoding("utf-8")
BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s: s[:-ord(s[len(s)-1:])]


def main():
    parser = argparse.ArgumentParser(description='漏洞挖掘机专用', version='0.0.1')
    parser.add_argument('-t', '--type', dest='type', help="运行的类型", choices=['e', 'd'], required=True)
    parser.add_argument('-i', '--input', dest='input', help="输入图片的路径", required=True)
    parser.add_argument('-o', '--output', dest='output', help="输出图片的路径，加密的时候使用到")
    parser.add_argument('-c', '--content', dest='content', help="需要加密的文本内容，加密时候使用到")
    parser.add_argument('-k', '--key', dest='key', help="内容进行加密的密钥", required=True)
    args = parser.parse_args()
    if not os.path.exists(args.input):
        print u'文件不存在：%s' % args.input
        return
    key = pad(args.key)
    # 加密
    if args.type == 'e':
        if args.output and args.content:
            print u'Starting encrypt'
            aes = AES.new(key, AES.MODE_CBC, key)
            txt = args.content
            data = pad(txt)
            content = aes.encrypt(data)
            content = base64.b64encode(content)
            Steganography.encode(args.input, args.output, content)
            print u'Done'
        else:
            print u"加密操作时候必须要output和content参数"
            return
    else:  # 解密
        print u'Starting decrypt'
        content = Steganography.decode(args.input)
        aes = AES.new(key, AES.MODE_CBC, key)
        txt = base64.b64decode(content)
        txt = unpad(aes.decrypt(txt))
        try:
            c = chardet.detect(txt)
            if c['confidence'] > 0.8:
                ret = txt.decode(c['encoding'])
            else:
                ret = txt.decode("gbk")
        except:
            ret = txt
        print u'Content: '
        print ret
        print u'Done'


if __name__ == '__main__':
    main()

```
加密我们使用了aes, 同时使用chardet检测内容的编码.
用法:
```bash
# 加密
python hunter.py -t e -i abc.jpg -o d.jpg -c 内容 -k 密钥
# 解密
python hunter.py -t d -i d.jpg -k 密钥
# 帮助
python hunter.py -h
```
运行前需要安装依赖的包:
```bash
pip install pycrypto chardet
```
