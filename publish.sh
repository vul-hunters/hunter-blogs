#!/usr/bin/env bash
# 记录当前位置
DIR=`pwd`
# 生成html
hugo -t redlounge
# 上传html代码
cd ../html/hunter-blogs
cp -rf ../.git ./.git
git add -A
git commit -m "$1"
git push
cd $DIR